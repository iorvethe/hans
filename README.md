<!--
    SPDX-FileCopyrightText: 2022 Felipe Kinoshita <kinofhek@gmail.cmo>
    SPDX-License-Identifier: CC0-1.0
-->

# Hans

Write markdown

![hans window](.gitlab/hans.png)

Hans lets you focus on writing markdown and it also helps you keep track of how many characters and words your document has as well as the estimated time it'll take to read it.

## Build Flatpak

To build a flatpak bundle of Hans use the following instructions:

```bash
$ git clone https://invent.kde.org/fhek/hans.git
$ cd hans
$ flatpak-builder --repo=repo build-dir --force-clean org.kde.hans.json
$ flatpak build-bundle repo hans.flatpak org.kde.hans
```

Now you can either double-click the `hans.flatpak` file to open it with
some app store (discover, gnome-software, etc...) or run:

```bash
$ flatpak install hans.flatpak
```
