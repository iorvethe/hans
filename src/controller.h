// SPDX-FileCopyrightText: 2022 Felipe Kinoshita <kinofhek@gmail.com>
// SPDX-License-Identifier: LGPL-2.1-or-later

#pragma once

#include <QObject>
#include <QUrl>

class Controller : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isEmptyFile MEMBER m_isEmptyFile NOTIFY isEmptyFileChanged)

public:
    explicit Controller(QObject* parent = nullptr);
    ~Controller() override;

    bool isEmptyFile();

    Q_INVOKABLE void clear();

    Q_INVOKABLE void open(QUrl filename);
    Q_SIGNAL void fileChanged(QString text, QString title);

    Q_INVOKABLE void save(QString text);
    Q_INVOKABLE void saveAs(QUrl filename, QString text);

    Q_SIGNAL void isEmptyFileChanged();

private:
    QString m_filename;
    bool m_isEmptyFile = true;
};
